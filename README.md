Hardened Firefox user settings.

How to install:
1. Visit about:profile  
2. Create new Firefox profile  
3. Open root directory of new profile  
4. Replace prefs.js  
  
Recommended addons:  
- https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/  
- https://addons.mozilla.org/en-US/firefox/addon/i-dont-care-about-cookies/  
- https://addons.mozilla.org/en-US/firefox/addon/dont-track-me-google1/  
- https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/  
- https://addons.mozilla.org/en-US/firefox/addon/clearurls/   
- https://addons.mozilla.org/en-US/firefox/addon/dont-track-me-google1/  
